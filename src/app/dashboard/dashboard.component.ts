import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.scss' ]
})
export class DashboardComponent implements OnInit {
  heroes: Hero[] = [];

  heroesQuantity = 'more'

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes.slice(0, 5));
  }
  toggleHeroesQuantity(): void {
    if(this.heroes.length < 6) {
      this.heroService.getHeroes()
        .subscribe(heroes => this.heroes = heroes.slice(0, 10));
      this.heroesQuantity = 'less'
    } else {
      this.heroService.getHeroes()
        .subscribe(heroes => this.heroes = heroes.slice(0, 5));
      this.heroesQuantity = 'more';
    }
  }
}
