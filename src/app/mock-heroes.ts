import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 1, name: 'Aragorn' },
  { id: 2, name: 'Frodo Baggins' },
  { id: 3, name: 'Laegolas' },
  { id: 4, name: 'Gandalf' },
  { id: 5, name: 'Gollum' },
  { id: 6, name: 'Sauron' },
  { id: 7, name: 'Arwen' },
  { id: 8, name: 'Gimli' },
  { id: 9, name: 'Galadriel' },
  { id: 10, name: 'Saruman' }
];
